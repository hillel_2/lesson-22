import datetime
import random
import socket
from time import sleep

IP = '127.0.0.1'
PORT = 23456

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((IP, PORT))
    sleep(1)
    nick = f'Nickname {int(random.random()*10)}'
    print(f'[{datetime.datetime.now().strftime("%d %b %y %H:%M:%S")}] Connected')
    s.sendall(bytes(nick, encoding='utf-8'))
    while True:
        data = input(f'{nick}: ')
        s.sendall(bytes(data, encoding='utf-8'))
        data2 = s.recv(1024)
        print(data2)
